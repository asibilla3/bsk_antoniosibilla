package tdd.training.bsk;

import java.util.ArrayList;

public class Game {

	/**
	 * It initializes an empty bowling game.
	 */
	private ArrayList<Frame> bowlingGame;
	private int firstBonusThrow;
	private int secondBonusThrow;
	
	public Game(){
		// To be implemented
		bowlingGame=new ArrayList<Frame>(10);
		firstBonusThrow=0;

	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		// To be implemented
		if(bowlingGame.size()<10) {
			
			bowlingGame.add(frame);
			
			
		}else {
			throw new BowlingException("il limite massimo di frame e' 10");
		}
			
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		// To be implemented
		return bowlingGame.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		// To be implemented
		this.firstBonusThrow=firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		// To be implemented
		this.secondBonusThrow= secondBonusThrow;;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		// To be implemented
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		// To be implemented
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		// To be implemented
		int s=0;

		for(int i=0;i<10;i++) {
			System.out.println("----------"+i+"------------");
			System.out.println(s);

			if(this.getFrameAt(i).isStrike()) {

				if(i>=9){
					this.getFrameAt(i).setBonus(this.getFirstBonusThrow()+this.getSecondBonusThrow());
				}else {

					if(this.getFrameAt(i+1).isStrike() && i<8) {

						if(this.getFrameAt(i+2).getFirstThrow()>0) {

							this.getFrameAt(i).setBonus(this.getFrameAt(i+1).getFirstThrow()+this.getFrameAt(i+2).getFirstThrow());
						}else {
							this.getFrameAt(i).setBonus(this.getFrameAt(i+1).getFirstThrow()+this.getFrameAt(i+2).getSecondThrow());
						}
					}else {
						
						if(i==8) {
							
							this.getFrameAt(i).setBonus(this.getFrameAt(i+1).getFirstThrow()+this.getFirstBonusThrow());
						}else {

							this.getFrameAt(i).setBonus(this.getFrameAt(i+1).getFirstThrow()+this.getFrameAt(i+1).getSecondThrow());
						}

					}
				}
				
				
				
				
				
				
				
			} else if(this.getFrameAt(i).isSpare() ) {
				
				if(i>=9){
					this.getFrameAt(i).setBonus(this.getFirstBonusThrow());
				}else {
					this.getFrameAt(i).setBonus(this.getFrameAt(i+1).getFirstThrow());
					
				}
				
			}
			
			s+=this.getFrameAt(i).getScore();
		}
		
		System.out.println(s);
		System.out.println("-----------------------");
		
		return s;	
	}

}

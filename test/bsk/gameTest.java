package bsk;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class gameTest {

	
	@Test
	public void TestInputGame() throws BowlingException {
		
		//ArrayList<Frame> frameList = new ArrayList<Frame>();
		Game inputGame= new Game();
		
	}
	
	
	
	@Test
	public void testGetFrameFirstThrow() throws BowlingException {
		
		//ArrayList<Frame> frameList = new ArrayList<Frame>();
		Game inputGame= new Game();
		
		inputGame.addFrame(new Frame(1,5));
		assertEquals(1,inputGame.getFrameAt(0).getFirstThrow());
		
		
	}
	
	
	@Test
	public void testGetFrameSecondThrow() throws BowlingException {
		
		Game inputGame= new Game();
		
		inputGame.addFrame(new Frame(1,5));
		assertEquals(5,inputGame.getFrameAt(0).getSecondThrow());
		
		
	}
	@Test (expected=BowlingException.class)
	
	public void addshouldRaiseException() throws BowlingException {
		
		Game inputGame= new Game();
		
		inputGame.addFrame(new Frame(1,5));
		inputGame.addFrame(new Frame(3,6));
		inputGame.addFrame(new Frame(7,2));
		inputGame.addFrame(new Frame(3,6));
		inputGame.addFrame(new Frame(4,4));
		inputGame.addFrame(new Frame(5,3));
		inputGame.addFrame(new Frame(3,3));
		inputGame.addFrame(new Frame(4,5));
		inputGame.addFrame(new Frame(8,1));
		inputGame.addFrame(new Frame(1,6));
		inputGame.addFrame(new Frame(1,6));
	}
	
	@Test
	public void testCalculateScore() throws BowlingException {
		
		Game inputGame= new Game();
		
		inputGame.addFrame(new Frame(3,6));
		inputGame.addFrame(new Frame(7,2));
		inputGame.addFrame(new Frame(3,6));
		inputGame.addFrame(new Frame(4,4));
		inputGame.addFrame(new Frame(5,3));
		inputGame.addFrame(new Frame(3,3));
		inputGame.addFrame(new Frame(4,5));
		inputGame.addFrame(new Frame(8,1));
		inputGame.addFrame(new Frame(1,6));
		inputGame.addFrame(new Frame(1,6));
		
		assertEquals(81,inputGame.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithSpare() throws BowlingException {
			
			Game inputGame= new Game();
			
			inputGame.addFrame(new Frame(3,6));
			inputGame.addFrame(new Frame(7,2));
			inputGame.addFrame(new Frame(3,6));
			inputGame.addFrame(new Frame(4,4));
			inputGame.addFrame(new Frame(5,3));
			inputGame.addFrame(new Frame(3,3));
			inputGame.addFrame(new Frame(4,5));
			inputGame.addFrame(new Frame(8,1));
			inputGame.addFrame(new Frame(4,6));
			inputGame.addFrame(new Frame(1,6));
			
			assertEquals(85,inputGame.calculateScore());
		}

	@Test
	public void testCalculateScoreWithStrike() throws BowlingException {
		
		Game inputGame= new Game();
		
		inputGame.addFrame(new Frame(3,6));
		inputGame.addFrame(new Frame(7,2));
		inputGame.addFrame(new Frame(3,6));
		inputGame.addFrame(new Frame(4,4));
		inputGame.addFrame(new Frame(5,3));
		inputGame.addFrame(new Frame(3,3));
		inputGame.addFrame(new Frame(4,5));
		inputGame.addFrame(new Frame(8,1));
		inputGame.addFrame(new Frame(10,0));
		inputGame.addFrame(new Frame(1,6));
		
		assertEquals(91,inputGame.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithStrikeAndSpare() throws BowlingException {
		
		Game inputGame= new Game();
		
		inputGame.addFrame(new Frame(10,0));
		inputGame.addFrame(new Frame(4,6));
		inputGame.addFrame(new Frame(7,2));
		inputGame.addFrame(new Frame(3,6));
		inputGame.addFrame(new Frame(4,4));
		inputGame.addFrame(new Frame(5,3));
		inputGame.addFrame(new Frame(3,3));
		inputGame.addFrame(new Frame(4,5));
		inputGame.addFrame(new Frame(8,1));
		inputGame.addFrame(new Frame(2,6));
		
		assertEquals(103,inputGame.calculateScore());
	}
	
	
	@Test
	public void testCalculateScoreWithMultipleStrikes() throws BowlingException {
		
		Game inputGame= new Game();
		
		inputGame.addFrame(new Frame(10,0));
		inputGame.addFrame(new Frame(10,0));
		inputGame.addFrame(new Frame(7,2));
		inputGame.addFrame(new Frame(3,6));
		inputGame.addFrame(new Frame(4,4));
		inputGame.addFrame(new Frame(5,3));
		inputGame.addFrame(new Frame(3,3));
		inputGame.addFrame(new Frame(4,5));
		inputGame.addFrame(new Frame(8,1));
		inputGame.addFrame(new Frame(2,6));
		
		assertEquals(112,inputGame.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithMultipleSpares() throws BowlingException {
		
		Game inputGame= new Game();
		
		inputGame.addFrame(new Frame(8,2));
		inputGame.addFrame(new Frame(5,5));
		inputGame.addFrame(new Frame(7,2));
		inputGame.addFrame(new Frame(3,6));
		inputGame.addFrame(new Frame(4,4));
		inputGame.addFrame(new Frame(5,3));
		inputGame.addFrame(new Frame(3,3));
		inputGame.addFrame(new Frame(4,5));
		inputGame.addFrame(new Frame(8,1));
		inputGame.addFrame(new Frame(2,6));
		
		assertEquals(98,inputGame.calculateScore());
	}
	
	@Test
	public void testGetFirstBonusThrow() throws BowlingException {
		Game inputGame= new Game();
		
		inputGame.setFirstBonusThrow(2);
		assertEquals(2,inputGame.getFirstBonusThrow());
		
	}
	
	
	@Test
	public void testCalculateScoreWithLastFrameSpare() throws BowlingException {
		
		Game inputGame= new Game();
		
		inputGame.addFrame(new Frame(1,5));
		inputGame.addFrame(new Frame(3,6));
		inputGame.addFrame(new Frame(7,2));
		inputGame.addFrame(new Frame(3,6));
		inputGame.addFrame(new Frame(4,4));
		inputGame.addFrame(new Frame(5,3));
		inputGame.addFrame(new Frame(3,3));
		inputGame.addFrame(new Frame(4,5));
		inputGame.addFrame(new Frame(8,1));
		inputGame.addFrame(new Frame(2,8));
		inputGame.setFirstBonusThrow(7);
		
		assertEquals(90,inputGame.calculateScore());
	}
	
	
	@Test
	public void testGetSecondBonusThrow() throws BowlingException {
		Game inputGame= new Game();
		
		inputGame.setSecondBonusThrow(2);
		assertEquals(2,inputGame.getSecondBonusThrow());
		
	}
	
	
	@Test
	public void testCalculateScoreWithLastFrameStrike() throws BowlingException {
		
		Game inputGame= new Game();
		
		inputGame.addFrame(new Frame(1,5));
		inputGame.addFrame(new Frame(3,6));
		inputGame.addFrame(new Frame(7,2));
		inputGame.addFrame(new Frame(3,6));
		inputGame.addFrame(new Frame(4,4));
		inputGame.addFrame(new Frame(5,3));
		inputGame.addFrame(new Frame(3,3));
		inputGame.addFrame(new Frame(4,5));
		inputGame.addFrame(new Frame(8,1));
		inputGame.addFrame(new Frame(10,0));
		
		inputGame.setFirstBonusThrow(7);
		inputGame.setSecondBonusThrow(2);
		
		assertEquals(92,inputGame.calculateScore());
	}
	
	
	@Test
	public void testCalculateScoreWithBestScore() throws BowlingException {
		
		Game inputGame= new Game();
		
		inputGame.addFrame(new Frame(10,0));
		inputGame.addFrame(new Frame(10,0));
		inputGame.addFrame(new Frame(10,0));
		inputGame.addFrame(new Frame(10,0));
		inputGame.addFrame(new Frame(10,0));
		inputGame.addFrame(new Frame(10,0));
		inputGame.addFrame(new Frame(10,0));
		inputGame.addFrame(new Frame(10,0));
		inputGame.addFrame(new Frame(10,0));
		inputGame.addFrame(new Frame(10,0));
		
		inputGame.setFirstBonusThrow(10);
		inputGame.setSecondBonusThrow(10);
		
		assertEquals(300,inputGame.calculateScore());
	}
	
	
	
	

	
	

	
	
	
	
	

}
 
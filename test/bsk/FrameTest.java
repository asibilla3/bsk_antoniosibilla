package bsk;

import static org.junit.Assert.*;

import tdd.training.bsk.*;
import org.junit.Test;


public class FrameTest {

	@Test
	public void shouldBeAFrame() throws Exception{
		int firstThrow=3;
		int secondThrow=2;
		Frame inputFrame = new Frame(firstThrow,secondThrow);
	}
	
	
	@Test(expected=BowlingException.class)
	
	public void frameShouldRaiseException() throws BowlingException{
		int firstThrow=3;
		int secondThrow=10;
		Frame inputFrame = new Frame(firstThrow,secondThrow);
	}
	
	@Test
	public void testGetFirstThrow() throws Exception{
		int firstThrow=3;
		int secondThrow=2;
		Frame inputFrame = new Frame(firstThrow,secondThrow);
		assertEquals(3,inputFrame.getFirstThrow());
		
	}
	
	
	@Test
	public void testGetSecondThrow() throws Exception{
		int firstThrow=3;
		int secondThrow=2;
		Frame inputFrame = new Frame(firstThrow,secondThrow);
		assertEquals(2,inputFrame.getSecondThrow());
		
	}
	
	@Test
	public void testGetScore() throws Exception{
		int firstThrow=3;
		int secondThrow=2;
		Frame inputFrame = new Frame(firstThrow,secondThrow);
		assertEquals(5,inputFrame.getScore());
		
	}
	
	
	@Test 
	public void testGetBonus() throws BowlingException {
			
			Frame secondFrame= new Frame(2,4);
			secondFrame.setBonus(secondFrame.getFirstThrow());
			
			assertEquals(2,secondFrame.getBonus());
			
			
	}
	
	
	@Test 
	public void frameShouldBeASpare() throws BowlingException {
			
			Frame inputFrame= new Frame(6,4);
			
			assertTrue(inputFrame.isSpare());
			
			
	}
	
	@Test 
	public void frameShouldNotBeASpare() throws BowlingException {
			
			Frame inputFrame= new Frame(2,4);
			
			assertFalse(inputFrame.isSpare());
			
			
	}
	
	@Test
	public void testGetScoreWithSpare() throws Exception{
		int firstThrow=4;
		int secondThrow=6;
		
		Frame inputFrame = new Frame(firstThrow,secondThrow);
		inputFrame.setBonus(5);
		
		assertEquals(15,inputFrame.getScore());
		
	}
	
	@Test
	public void frameShouldBeAStrike() throws Exception{
		int firstThrow=10;
		int secondThrow=0;
		
		Frame inputFrame = new Frame(firstThrow,secondThrow);
	
		
		assertTrue(inputFrame.isStrike());
		
	}
	
	@Test
	public void frameShouldNotBeAStrike() throws Exception{
		int firstThrow=6;
		int secondThrow=0;
		
		Frame inputFrame = new Frame(firstThrow,secondThrow);
	
		
		assertFalse(inputFrame.isStrike());
	}
	
	
	@Test
	public void testGetScoreWithStrike() throws BowlingException {
		
		int firstThrow=10;
		int secondThrow=0;
		
		Frame inputFrame = new Frame(firstThrow,secondThrow);
		inputFrame.setBonus(2+4);
		
		assertEquals(16,inputFrame.getScore());
		
	}
	
	
	
	
	

}
